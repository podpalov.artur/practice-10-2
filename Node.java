public class Node {
    private String val;
    private Node next;
    public Node(String _val) {
        val = _val;
        next = null;
    }
    public String getVal() {
        return val;
    }
    public void setVal(String val) {
        this.val = val;
    }
    public Node getNext() {
        return next;
    }
    public void setNext(Node next) {
        this.next = next;
    }
}
