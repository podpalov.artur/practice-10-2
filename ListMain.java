public class ListMain {
    public static void main(String[] args) {
        List list = new List();
        list.pushBack("one");
        list.pushBack("two");
        list.pushBack("three");
        list.pushBack("four");
        list.pushBack("five");
        list.insert(3, "0101000100");
        list.delete(5);
        list.replace(2, "0xA28");
        list.print();
        System.out.println(list.size());
    }
}
