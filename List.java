public class List {
    private Node first;
    private Node last;

    public List() {
        first = null;
        last = null;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void pushBack(String _val) {
        Node p = new Node(_val);
        if (isEmpty()) {
            first = p;
            last = p;
            return;
        }
        last.setNext(p);
        last = p;
    }

    public int size() {
        int count = 0;
        Node current = first;
        while (current != last) {
            count++;
            current = current.getNext();
        }
        count++;
        return count;
    }

    public String get(int index) {
        if (index < 0 || index > size() - 1) {
            throw new RuntimeException("Index is invalid.");
        }
        Node current = first;
        for (int i = 0; i < index; i++) {
            current = current.getNext();
        }
        return current.getVal();
    }

    public void insert(int index, String _val) {
        if (index < 0 || index > size() - 1) {
            throw new RuntimeException("Index is invalid.");
        }
        Node added = new Node(_val);
        if (index == 0) {
            added.setNext(first);
            first = added;
        } else {
            Node current = first;
            for (int i = 0; i < index - 1; i++) {
                current = current.getNext();
            }
            added.setNext(current.getNext());
            current.setNext(added);
        }
    }

    public void delete(int index) {
        if (index < 0 || index > size() - 1) {
            throw new RuntimeException("Index is invalid.");
        }
        if (index == 0) {
            first = first.getNext();
        } else if (index == size() - 1) {
            Node current = first;
            Node prev = last;
            while (current != last) {
                prev = current;
                current = current.getNext();
            }
            last = prev;
        } else {
            Node current = first;
            for (int i = 0; i < index - 1; i++) {
                current = current.getNext();
            }
            current.setNext(current.getNext().getNext());
        }
    }

    public void replace(int index, String _val) {
        if (index < 0 || index > size() - 1) {
            throw new RuntimeException("Index is invalid.");
        }
        Node current = first;
        for (int i = 0; i < index; i++) {
            current = current.getNext();
        }
        current.setVal(_val);
    }

    public void print() {
        Node current = first;
        while (current != last) {
            System.out.println(current.getVal());
            current = current.getNext();
        }
        System.out.println(current.getVal());
    }
}


